[![](https://jitpack.io/v/com.gitlab.biom4st3r/whatsintheboxapi.svg)](https://jitpack.io/#com.gitlab.biom4st3r/whatsintheboxapi)
# Whats in the Box
Is a basic api allowing the Client to receive information about LootTables on the server

Implementation: https://gitlab.com/biom4st3r/whatsintheboximpl (HWYLAPlugin.java)

## Versions
<table>
<tr>
<td><b>Version</b></td>
<td><b>Description</b></td>
</tr>
<tr>
<td>0.1.0</td>
<td>Initial Release</td>
</tr>
<tr>
<td>0.1.3</td>
<td>Exposed</td>
</tr>
</table>

## Adding to Project
If you using fabric-example-mod as a base you'll need to add the follow to your build.gradle
```
repositories 
{  
	maven { url 'https://jitpack.io' }
}
```
and add the follow to the dependencies block
```
modCompile "com.gitlab.biom4st3r:WhatsInTheBoxAPI:{version}"
include "com.gitlab.biom4st3r:WhatsInTheBoxAPI:{version}"
```

## Run down
* The Main method you'll be needing is ```WhatsInTheBox#requestLootTable(Identifier lootTableId)```. 

 This will store a ```LootTable``` in ```LootTableStorage``` or ```LootTable.EMPTY``` if it wasn't a valid id.
<p>

* If the server permits it you can also use ```WhatsInTheBox#requestAllLootTableIds()```.

This will store all LootTableIds in ```LootTableStorage``` with null LootTables



## Note
* This mod also enables ```LootableContainerBlockEntities``` to store there ```LootTableId``` tag with the client(configurable). So you won't need to request that from the server; Just ask that client sideded BlockEntity




## License
Check Licence file