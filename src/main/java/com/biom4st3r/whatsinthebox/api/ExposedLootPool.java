package com.biom4st3r.whatsinthebox.api;

import java.util.function.BiFunction;
import java.util.function.Predicate;

import net.fabricmc.fabric.api.loot.v1.FabricLootPool;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTableRange;
import net.minecraft.loot.UniformLootTableRange;
import net.minecraft.loot.condition.LootCondition;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.entry.LootEntry;
import net.minecraft.loot.function.LootFunction;

/**
 * interfaces inherited by all {@link LootPool}'s. <p>
 * Exposes fields and provides easy access to other Exposed Objects
 */
public interface ExposedLootPool {

    public LootEntry[] getEntries();
    public LootCondition[] getLootConditions();
    public Predicate<LootContext> getPredicate();
    public LootFunction[] getLootFunctions();
    public BiFunction<ItemStack, LootContext, ItemStack> getFunction();
    public LootTableRange getRollsRange();
    public UniformLootTableRange getBonusRollsRange();

    default public FabricLootPool asFabric()
    {
        return (FabricLootPool)this;
    }
    // default public LootPool asVanilla()
    // {
    //     return this.asFabric().asVanilla();
    // }

    default public ExposedEntry[] getExposedEntries()
    {
        return (ExposedEntry[]) this.getEntries();
    }
}