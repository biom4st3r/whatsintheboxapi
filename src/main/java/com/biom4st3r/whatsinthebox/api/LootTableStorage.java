package com.biom4st3r.whatsinthebox.api;

import java.util.HashMap;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.loot.LootTable;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class LootTableStorage extends HashMap<Identifier,LootTable> 
{
    private static final long serialVersionUID = -5095510925359710532L;
    public static final Object lock = new Object();
    private static LootTableStorage instance;

    private LootTableStorage()
    {
    }

    public static LootTableStorage instance()
    {
        synchronized(lock)
        {
            if(instance == null) instance = new LootTableStorage();
            return instance;
        }
    }
    /**
     * Wrapper for {@link LootTableStorage#get(Object)} that casts to {@link ExposedLootTable} <p>
     * , but cast to expose fields
     * @param id of the lootTable
     * @return (AccessableLootTable)this.get(id) or null if {@link LootTable} was null
     */
    public ExposedLootTable getExposedLootTable(Identifier id)
    {
        return (ExposedLootTable)this.get(id);
    }
    /**
     * 
     * @param id of the lootTable
     * @return ExposedLootPool[] from {@link ExposedLootTable#getExposedLootPools()}
     */
    public ExposedLootPool[] getPoolsFromTable(Identifier id)
    {
        return this.getExposedLootTable(id).getExposedLootPools();
    }
    public boolean isEmpty(Identifier id)
    {
        return this.get(id) == LootTable.EMPTY;
    }
    public boolean isNull(Identifier id)
    {
        return this.get(id) == null;
    }
}