package com.biom4st3r.whatsinthebox.api;

import com.biom4st3r.whatsinthebox.mxn.ExposedItemEntry;
import com.biom4st3r.whatsinthebox.mxn.ExposedLootTableEntry;

import net.minecraft.loot.entry.ItemEntry;
import net.minecraft.loot.entry.LootEntry;
import net.minecraft.loot.entry.LootTableEntry;

/**
 * Hub for retrieving Exposed version of entries. 
 * all {@link ExposedEntry#asOtherType()} Functions will return null if they aren't of that type
 * 
 */
public interface ExposedEntry {
    default public LootEntry asVanilla()
    {
        return (LootEntry) this;
    }
    /**
     * @return Checks if this LootEntry is an instanceof {@link ItemEntry}<p>
     * if it's not: returns null
     */
    default public ExposedItemEntry asExposedItemEntry()
    {
        return (this instanceof ItemEntry ? (ExposedItemEntry) this : null);
        //LootTableEntry
    }
    /**
     * 
     * @return checkks if this LootEntry is an instanceof {@link LootTableEntry}<p>
     * if it's not: returns null
     */
    default public ExposedLootTableEntry asExposedLootTableEntry()
    {
        return (this instanceof LootTableEntry ? (ExposedLootTableEntry) this : null);
    }

    // default public AlternativeEntry asExposedAlternatieEntry()
    // {
    //     return (this instanceof AlternativeEntry ? (AlternativeEntry) this :  null);
    // }
}