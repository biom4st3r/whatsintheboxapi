package com.biom4st3r.whatsinthebox.api;

import java.util.function.BiFunction;

import com.biom4st3r.whatsinthebox.mxn.LootTableMxn;

import net.fabricmc.fabric.api.loot.v1.FabricLootPool;
import net.fabricmc.fabric.api.loot.v1.FabricLootSupplier;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextType;
import net.minecraft.loot.function.LootFunction;

/**
 * Mxn wrapper to expose fields on a {@link LootTable}<p>
 * extention of {@link LootTableMxn}
 */
public interface ExposedLootTable  
{


    public LootContextType getType();

    public LootPool[] getLootPools();

    public LootFunction[] getLootFunctions();

    public BiFunction<ItemStack, LootContext, ItemStack> getCombinedFunction();

    default public ExposedLootPool[] getExposedLootPools()
    {
        return (ExposedLootPool[]) this.getLootPools();
    }
    default public FabricLootPool[] getFabricLootPools()
    {
        return (FabricLootPool[]) this.getLootPools();
    }
    default public FabricLootSupplier asFabric()
    {
        return (FabricLootSupplier) this;
    }
    // default public LootTable asVanilla()
    // {
    //     return this.asFabric().asVanilla();
    // }
    default public boolean isEmpty()
    {
        return this == LootTable.EMPTY;
    }
    
}