package com.biom4st3r.whatsinthebox.mxn;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.loot.entry.LootTableEntry;
import net.minecraft.util.Identifier;

/**
 * ExposedLootTableEntry
 */
@Mixin(LootTableEntry.class)
public interface ExposedLootTableEntry extends ExposedLeafEntry {

    @Accessor("id")
    public Identifier getIdentifier();
    
}