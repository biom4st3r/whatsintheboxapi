package com.biom4st3r.whatsinthebox.mxn;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.item.Item;
import net.minecraft.loot.entry.ItemEntry;

/**
 * ExposedItemEntry
 */
@Mixin(ItemEntry.class)
public interface ExposedItemEntry extends ExposedLeafEntry {
    @Accessor("item")
    public Item getItem();
}