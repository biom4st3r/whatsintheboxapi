package com.biom4st3r.whatsinthebox.mxn;

import java.util.function.BiFunction;
import java.util.function.Predicate;

import com.biom4st3r.whatsinthebox.api.ExposedLootPool;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTableRange;
import net.minecraft.loot.UniformLootTableRange;
import net.minecraft.loot.condition.LootCondition;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.entry.LootEntry;
import net.minecraft.loot.function.LootFunction;

@Mixin(LootPool.class)
public abstract class LootPoolMxn implements ExposedLootPool
{

    @Shadow @Final private LootEntry[] entries;
    @Shadow @Final private LootCondition[] conditions;
    @Shadow @Final private Predicate<LootContext> predicate;
    @Shadow @Final private LootFunction[] functions;
    @Shadow @Final private BiFunction<ItemStack, LootContext, ItemStack> javaFunctions;
    @Shadow @Final private LootTableRange rollsRange;
    @Shadow @Final private UniformLootTableRange bonusRollsRange;
    
    @Override
    public LootEntry[] getEntries()
    {
        return this.entries;
    }
    @Override
    public LootCondition[] getLootConditions()
    {
        return this.conditions;
    }
    @Override
    public Predicate<LootContext> getPredicate()
    {
        return this.predicate;
    }
    @Override
    public LootFunction[] getLootFunctions()
    {
        return this.functions;
    }
    @Override
    public BiFunction<ItemStack, LootContext, ItemStack> getFunction()
    {
        return this.javaFunctions;
    }
    @Override
    public LootTableRange getRollsRange()
    {
        return this.rollsRange;
    }
    @Override
    public UniformLootTableRange getBonusRollsRange()
    {
        return this.bonusRollsRange;
    }

}
