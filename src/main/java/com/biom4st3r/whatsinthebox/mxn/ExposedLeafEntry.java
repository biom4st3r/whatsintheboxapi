package com.biom4st3r.whatsinthebox.mxn;

import java.util.function.BiFunction;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.entry.LeafEntry;

/**
 * ExposedLeafEntry
 */
@Mixin(LeafEntry.class)
public interface ExposedLeafEntry extends ExposedLootEntry {

    @Accessor("weight")
    public int getWeight();
    @Accessor("quality")
    public int getQuality();
    @Accessor("compiledFunctions")
    public  BiFunction<ItemStack, LootContext, ItemStack>  getCompiledFunctions();

}