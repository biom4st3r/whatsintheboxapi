package com.biom4st3r.whatsinthebox.mxn;

import com.biom4st3r.whatsinthebox.WhatsInTheBox;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixinTest extends LivingEntity {
    protected PlayerEntityMixinTest(EntityType<? extends LivingEntity> entityType_1, World world_1) {
        super(entityType_1, world_1);
        // TODO Auto-generated constructor stub
    }

    @Inject(at = @At("HEAD"),method="jump")
    public void myInjection(CallbackInfo ci)
    {
        if(this.world.isClient) WhatsInTheBox.doTest2();
    }

}