package com.biom4st3r.whatsinthebox.mxn;

import java.util.function.BiFunction;

import com.biom4st3r.whatsinthebox.api.ExposedLootTable;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextType;
import net.minecraft.loot.function.LootFunction;

/**
 * LootTableMxn
 */
@Mixin(LootTable.class)
public abstract class LootTableMxn implements ExposedLootTable {

    @Shadow
    @Final
    private LootContextType type;
    @Shadow
    @Final
    private LootPool[] pools;
    @Shadow
    @Final
    private LootFunction[] functions;
    @Shadow
    @Final
    private BiFunction<ItemStack, LootContext, ItemStack> combinedFunction;

    @Override
    public LootContextType getType() {
        return this.type;
    }

    @Override
    public LootPool[] getLootPools() {
        return this.pools;
    }

    @Override
    public LootFunction[] getLootFunctions() {
        return this.functions;
    }

    @Override
    public BiFunction<ItemStack, LootContext, ItemStack> getCombinedFunction() {
        return this.combinedFunction;
    }
}
