package com.biom4st3r.whatsinthebox.mxn;

import java.util.Map;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.loot.LootManager;
import net.minecraft.loot.LootTable;
import net.minecraft.util.Identifier;

/**
 * LootManagerMxn
 */
@Mixin(LootManager.class)
public interface LootManagerMxn {

    @Accessor("suppliers")
    public Map<Identifier, LootTable> getLootTableIds();

}