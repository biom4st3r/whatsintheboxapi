package com.biom4st3r.whatsinthebox.mxn;

import java.util.function.Predicate;

import com.biom4st3r.whatsinthebox.api.ExposedEntry;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.loot.condition.LootCondition;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.entry.LootEntry;

/**
 * ExposedLootEntry
 */

@Mixin(LootEntry.class)
public interface ExposedLootEntry extends ExposedEntry {

    @Accessor("conditions")
    public LootCondition[] getConditions();
    @Accessor("conditionPredicate")
    public Predicate<LootContext> getConditionPredicate();

    
}