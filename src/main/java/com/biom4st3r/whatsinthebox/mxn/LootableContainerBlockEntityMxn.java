package com.biom4st3r.whatsinthebox.mxn;

import com.biom4st3r.whatsinthebox.WhatsInTheBox;

import org.spongepowered.asm.mixin.Mixin;

import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.LockableContainerBlockEntity;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.client.network.packet.BlockEntityUpdateS2CPacket;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.registry.Registry;

@Mixin(LootableContainerBlockEntity.class)
//BarrelBlockEntity
public abstract class LootableContainerBlockEntityMxn extends LockableContainerBlockEntity implements BlockEntityClientSerializable {

    protected LootableContainerBlockEntityMxn(BlockEntityType<?> blockEntityType_1) {
        super(blockEntityType_1);
    }

    @Override
    public BlockEntityUpdateS2CPacket toUpdatePacket() {
        CompoundTag ct = this.toTag(new CompoundTag());
        ct.remove("Lock"); 
        if(!WhatsInTheBox.config.server_Share_BlockEntity_Loot_Table_Id_With_Client)
        {
            ct.remove("LootTable");
        }
        if(!WhatsInTheBox.config.server_Share_BlockEntity_Loot_Table_Seed_With_Client)
        {
            ct.remove("LootTableSeed");
        }
        // System.out.println(ct);
        
        //ClientPlayNetworkHandler
        return new BlockEntityUpdateS2CPacket(this.pos,Registry.BLOCK_ENTITY.getRawId(this.getType()),ct);        
    }

    @Override
    public void fromClientTag(CompoundTag tag) {
        this.fromTag(tag);
    }

    @Override
    public CompoundTag toClientTag(CompoundTag ct) {
        this.toTag(ct);
        ct.remove("Lock"); 
        if(!WhatsInTheBox.config.server_Share_BlockEntity_Loot_Table_Id_With_Client)
        {
            ct.remove("LootTable");
        }
        if(!WhatsInTheBox.config.server_Share_BlockEntity_Loot_Table_Seed_With_Client)
        {
            ct.remove("LootTableSeed");
        }
        return ct;
    }

}