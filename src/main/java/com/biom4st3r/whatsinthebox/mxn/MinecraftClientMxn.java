package com.biom4st3r.whatsinthebox.mxn;

import com.biom4st3r.whatsinthebox.api.LootTableStorage;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.MinecraftClient;

@Mixin(MinecraftClient.class)
public abstract class MinecraftClientMxn
{

    @Inject(at = @At("HEAD"),method="disconnect")
    public void onDisconnect(CallbackInfo ci)
    {
        LootTableStorage.instance().clear();
        // OtherClientPlayerEntity

    }

}