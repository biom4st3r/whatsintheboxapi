package com.biom4st3r.whatsinthebox;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import com.biom4st3r.whatsinthebox.api.ExposedEntry;
import com.biom4st3r.whatsinthebox.api.ExposedLootPool;
import com.biom4st3r.whatsinthebox.api.ExposedLootTable;
import com.biom4st3r.whatsinthebox.api.LootTableStorage;
import com.biom4st3r.whatsinthebox.mxn.ExposedItemEntry;
import com.biom4st3r.whatsinthebox.mxn.ExposedLootTableEntry;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.loot.v1.FabricLootPool;
import net.fabricmc.fabric.api.loot.v1.FabricLootSupplier;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.loot.BinomialLootTableRange;
import net.minecraft.loot.ConstantLootTableRange;
import net.minecraft.loot.LootManager;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.UniformLootTableRange;
import net.minecraft.loot.condition.LootCondition;
import net.minecraft.loot.condition.LootConditions;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.entry.LootEntries;
import net.minecraft.loot.entry.LootEntry;
import net.minecraft.loot.function.LootFunction;
import net.minecraft.loot.function.LootFunctions;
import net.minecraft.util.BoundedIntUnaryOperator;
import net.minecraft.util.Identifier;

/**
 * ModInit
 */
public class WhatsInTheBox implements ModInitializer, ClientModInitializer {

    // net.fabricmc.fabric.mixin.loot.MixinLootPool
    // public static BioLogger logger;

    /**
     * Ripped straight out of {@link LootManager#GSON}
     * <p>
     * you can use this object to Serialize and Deserialize LootTables
     */
    public static final Gson LootTableConverter = (new GsonBuilder())
            .registerTypeAdapter(UniformLootTableRange.class, new UniformLootTableRange.Serializer())
            .registerTypeAdapter(BinomialLootTableRange.class, new BinomialLootTableRange.Serializer())
            .registerTypeAdapter(ConstantLootTableRange.class, new ConstantLootTableRange.Serializer())
            .registerTypeAdapter(BoundedIntUnaryOperator.class, new BoundedIntUnaryOperator.Serializer())
            .registerTypeAdapter(LootPool.class, new LootPool.Serializer())
            .registerTypeAdapter(LootTable.class, new LootTable.Serializer())
            .registerTypeHierarchyAdapter(LootEntry.class, new LootEntries.Serializer())
            .registerTypeHierarchyAdapter(LootFunction.class, new LootFunctions.Factory())
            .registerTypeHierarchyAdapter(LootCondition.class, new LootConditions.Factory())
            .registerTypeHierarchyAdapter(LootContext.EntityTarget.class, new LootContext.EntityTarget.Serializer())
            .create();
    public static String MODID = "witb";

    // public static void enableDebugLogs()
    // {
    // Biow0rks.DEBUG_MODE = true;
    // }

    /**
     * All information received about LootTables will be Stored in
     * {@link LootTableStorage#instance()}. This stores the requested lootTable in
     * {@link LootTableStorage}.
     * <p>
     * If the lootTableId wasn't found on the server {@link LootTable#EMPTY} will be
     * stored instead
     * 
     * @param lootTableId
     */
    @Environment(EnvType.CLIENT)
    public static void requestLootTable(Identifier lootTableId) {
        ClientSidePacketRegistry.INSTANCE.sendToServer(PacketInit.CLIENT.createRequestLootTable(lootTableId));
    }

    /**
     * All information received about LootTables will be Stored in
     * {@link LootTableStorage#instance()}.
     * <p>
     * This will request all LootTableIds from the server and store them with a
     * value of null in {@link LootTableStorage}.
     * <p>
     * This is non-destructive and will not override previous existing keys/caches.
     * <p>
     * You can use {@link LootTableStorage#keySet()} to get all keys
     */
    @Environment(EnvType.CLIENT)
    public static void requestAllLootTableIds() {
        ClientSidePacketRegistry.INSTANCE.sendToServer(PacketInit.CLIENT.createRequestAllLootTableIds());
    }

    public static WhatsInTheBoxConfig config;

    @Override
    public void onInitializeClient() {
        PacketInit.clientPacketInit();

    }

    public static void doTest2() {
        if (LootTableStorage.instance().size() == 0)
            requestAllLootTableIds();
        new Thread(() -> {
            while (LootTableStorage.instance().size() == 0) {
                System.out.println("waiting for entries");
                ;
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            LootTableStorage.instance().keySet().forEach((id) -> {
                requestLootTable(id);
                while (LootTableStorage.instance().getExposedLootTable(id) == null) {
                    System.out.println("waiting for not null");
                    ;
                }
            });

            Iterator<Identifier> iter = LootTableStorage.instance().keySet().iterator();
            Identifier id;
            while (iter.hasNext()) {
                id = iter.next();
                System.out.print(id);
                ExposedLootTable elt = LootTableStorage.instance().getExposedLootTable(id);
                if (!elt.isEmpty() && !(elt == null)) {
                    FabricLootSupplier fls = elt.asFabric();
                    if(fls == null) System.out.println("fls is null");
                    LootTable lt = fls.asVanilla();
                    if(lt == null) System.out.println("lt is null");
                    LootPool[] lp = elt.getLootPools();
                    if(lp == null) System.out.println("lp is null");
                    ExposedLootPool[] elp = elt.getExposedLootPools();
                    if(elp == null) System.out.println("elp is null");
                    FabricLootPool[] flp = elt.getFabricLootPools();
                    if(flp == null) System.out.println("flp is null");
                    if (fls != null && lt != null && lp != null && elp != null && flp != null) {
                        if (elp.length != 0) {
                            System.out.println(" " + elp.length);
                            ExposedEntry[] ee = elp[0].getExposedEntries();
                            if(ee.length != 0)
                            {
                                ExposedItemEntry eie = ee[0].asExposedItemEntry();
                                ExposedLootTableEntry ette = ee[0].asExposedLootTableEntry();
                                LootEntry le = ee[0].asVanilla();
                                if(eie != null)
                                {
                                    System.out.println(String.format("Name: %s Quality: %s  Weight: %s", eie.getItem().getName().asFormattedString(),eie.getQuality(), eie.getWeight()));
                                }
                                else if(ette != null)
                                {
                                    System.out.println("lootTable: " + ette.getIdentifier() + " Weight: " + ette.getWeight() + " Quality: " + ette.getQuality() + "\n");
                                }
                                else
                                {
                                    System.out.println("not ItemEntry or LootTableEntry\n" + le.getClass().getName() + "\n");
                                }
                            }
                        }
                    }
                    else
                    {
                        System.out.println("table or pools null\n\n");
                    }
                    

                }
                else if(elt.isEmpty()) System.out.println("elt is empty");
                else System.out.println("elt is null");
            }
            Thread.yield();
        }).start();
    }

    @Override
    public void onInitialize() 
    {
        LootTable.EMPTY.equals(null);
        PacketInit.serverPacketInit();
        // logger = new BioLogger("WhatsInTheBoxAPI");
        File file = new File(FabricLoader.getInstance().getConfigDirectory().getPath(), "WhatsInTheBoxConfig.json");
		try {
			FileReader fr = new FileReader(file);
			config = new Gson().fromJson(fr, WhatsInTheBoxConfig.class);
			FileWriter fw = new FileWriter(file);
			fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
			fw.close();
		} catch (IOException e) {
			config = new WhatsInTheBoxConfig();
			try {
				FileWriter fw = new FileWriter(file);
				fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
				fw.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
        }
        // IdentifiableResourceReloadListener listener = new IdentifiableResourceReloadListener(){
        
        //     @Override
        //     public CompletableFuture<Void> reload(Synchronizer var1, ResourceManager var2, Profiler var3, Profiler var4,
        //             Executor var5, Executor var6) {
        //                 return CompletableFuture.runAsync(()->
        //                 {
        //                     LootTableStorage.instance().clear();
        //                 });
        //     }
        
        //     @Override
        //     public Identifier getFabricId() {
        //         return new Identifier("witb", "clearloottablestorage");
        //     }
        // };
        // ResourceManagerHelperImpl.get(ResourceType.CLIENT_RESOURCES).registerReloadListener(listener);
        
    }
}