package com.biom4st3r.whatsinthebox;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.biom4st3r.whatsinthebox.api.LootTableStorage;
import com.biom4st3r.whatsinthebox.mxn.LootManagerMxn;
import com.google.common.collect.Lists;

import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.minecraft.client.network.packet.CustomPayloadS2CPacket;
import net.minecraft.loot.LootManager;
import net.minecraft.loot.LootTable;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.network.packet.CustomPayloadC2SPacket;
import net.minecraft.util.Identifier;
import net.minecraft.util.PacketByteBuf;

/**
 * PacketInit
 */
public class PacketInit {

    public static final Identifier REQUEST_LOOT_TABLE = new Identifier(WhatsInTheBox. MODID, "req_loot_table");
    public static final Identifier SEND_LOOT_TABLE = new Identifier(WhatsInTheBox.MODID, "snd_loot_table");

    public static final Identifier REQUEST_ALL_LOOT_TABLE_IDS = new Identifier(WhatsInTheBox.MODID, "req_all_table_id");
    public static final Identifier SEND_ALL_LOOT_TABLE_IDS = new Identifier(WhatsInTheBox.MODID, "snd_all_table_id");



    public static class CLIENT {
        public static CustomPayloadC2SPacket createRequestLootTable(Identifier lootTableId) {
            PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer());
            pbb.writeIdentifier(lootTableId);
            return new CustomPayloadC2SPacket(REQUEST_LOOT_TABLE, pbb);
        }

        public static CustomPayloadC2SPacket createRequestAllLootTableIds() {
            return new CustomPayloadC2SPacket(REQUEST_ALL_LOOT_TABLE_IDS, new PacketByteBuf(Unpooled.buffer()));
        }

    }

    public static class SERVER {
        public static CustomPayloadS2CPacket createSendLootTable(Identifier id, String json) {
            PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer());
            pbb.writeIdentifier(id);
            pbb.writeString(json);
            return new CustomPayloadS2CPacket(SEND_LOOT_TABLE, pbb);
        }

        public static CustomPayloadS2CPacket createSendAllLootTableIds(Set<Identifier> ids) {
            PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer());
            pbb.writeInt(ids.size());
            Iterator<Identifier> iter = ids.iterator();
            while (iter.hasNext()) {
                pbb.writeIdentifier(iter.next());
            }
            return new CustomPayloadS2CPacket(SEND_ALL_LOOT_TABLE_IDS, pbb);
        }
    }

    @Environment(EnvType.CLIENT)
    public static void clientPacketInit()
    {
        // System.out.println("registered SEND LOOT TABLE");
        ClientSidePacketRegistry.INSTANCE.register(SEND_LOOT_TABLE, (context, pbb) -> {
            
            Identifier lootTableId = pbb.readIdentifier();
            String json = pbb.readString();
            LootTable table = WhatsInTheBox.LootTableConverter.fromJson(json, LootTable.class);
            LootTableStorage.instance().put(lootTableId, table);
            // System.out.println("Received lootTable %s. Table is %sEmpty " + lootTableId + (table == LootTable.EMPTY ? "" : "not "));
        });
        // System.out.println("registered SEND LOOT ALL TABLE IDS");
        ClientSidePacketRegistry.INSTANCE.register(SEND_ALL_LOOT_TABLE_IDS, (context,pbb)->
        {
            // System.out.println("Receiving all table ids");
            int size = pbb.readInt();
            List<Identifier> ids = Lists.newArrayList();
            for (int i = 0; i < size; i++) {
                ids.add(pbb.readIdentifier());
            }
            context.getTaskQueue().execute(()->
            {
                for(Identifier id : ids)
                {
                    
                    if (!LootTableStorage.instance().containsKey(id)) {
                        // System.out.println("Received %s" +id);
                        LootTableStorage.instance().put(id, null);
                    }
                    else
                    {
                        // System.out.println("Received %s. Already exists in Storage. Skipping " + id);
                    }
                }
            });
            
        });
    }

    public static void serverPacketInit()
    {
        ServerSidePacketRegistry.INSTANCE.register(REQUEST_LOOT_TABLE, (context, pbb) -> {
            
            Identifier id = pbb.readIdentifier();
            // System.out.println(String.format("Client requested %s", id));
            context.getTaskQueue().execute(() -> {
                if(Arrays.asList(WhatsInTheBox.config.server_Restricted_LootTables).contains(id.toString()))
                {
                    // System.out.println(String.format("Skipping Restricted id: %s", id));
                    ((ServerPlayerEntity) context.getPlayer()).networkHandler
                    .sendPacket(SERVER.createSendLootTable(id, WhatsInTheBox.LootTableConverter.toJson(LootTable.EMPTY)));
                    return;
                }
                LootTable table = context.getPlayer().getServer().getLootManager().getSupplier(id);
                String json = LootManager.toJson(table).toString();
                ServerSidePacketRegistry.INSTANCE.sendToPlayer(context.getPlayer(), SERVER.createSendLootTable(id, json));
            });
        });
        ServerSidePacketRegistry.INSTANCE.register(REQUEST_ALL_LOOT_TABLE_IDS, (context, pbb) -> {
            if(!WhatsInTheBox.config.server_Permit_Request_All)
            {
                // System.out.println("Rejected client request for All_Loot_Table_IDS as per server config");
                return;
            }
            context.getTaskQueue().execute(() -> {
                Map<Identifier, LootTable> ids = ((LootManagerMxn) context.getPlayer().getServer().getLootManager())
                        .getLootTableIds();
                // System.out.println("Sending all loot tables ids");
                ((ServerPlayerEntity)context.getPlayer()).networkHandler.sendPacket(SERVER.createSendAllLootTableIds(ids.keySet()));
            });
        });

    }
}