package com.biom4st3r.whatsinthebox;

/**
 * WhatsInTheBoxConfig
 */
public class WhatsInTheBoxConfig {

    public boolean server_Permit_Request_All;
    
    public String[] server_Restricted_LootTables;

    public boolean server_Share_BlockEntity_Loot_Table_Id_With_Client;

    public boolean server_Share_BlockEntity_Loot_Table_Seed_With_Client;

    public WhatsInTheBoxConfig()
    {
        server_Permit_Request_All = true;
        server_Restricted_LootTables = new String[]{""};
        server_Share_BlockEntity_Loot_Table_Id_With_Client = true;
        server_Share_BlockEntity_Loot_Table_Seed_With_Client = true;
    }
}